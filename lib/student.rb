class Student

  def initialize(first_name, last_name)
    @first_name = first_name
    @last_name = last_name
    @courses = []
  end


  attr_reader :first_name, :last_name, :courses

  def name
    "#{@first_name} #{@last_name}"
  end

  def courses
    @courses
  end

  def enroll(course)
    if has_conflict?(course)
      raise "New course would conflict with schedule"
    elsif !courses.include?(course)
      courses << course
      course.students << self
    end
  end

  def has_conflict?(course)
    @courses.each do |current_course|
      return true if current_course.conflicts_with?(course)
    end
    false
  end
  def course_load
    course_load_hash = Hash.new(0)
    courses.each do |course|
      course_load_hash[course.department] += course.credits
    end
    course_load_hash
  end

end
